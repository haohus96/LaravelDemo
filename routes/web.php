<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\TheLoai;
Route::get('/', function () {
    return redirect('admin/theloai/danhsach') ;
    //view('admin.layout.index');
});
Route::get('thu',function(){
	$TheLoai = TheLoai::find(1);
	foreach($TheLoai->loaitin as $loaitin){
		echo $loaitin->Ten . "<br>" ;
	}
});
Route::get('test',function(){
	return view('home');
});
Route::get('admin/dangnhap','UserController@getDangnhapAdmin') ;
Route::post('admin/dangnhap','UserController@postDangnhapAdmin') ;
Route::get('admin/logout','UserController@getDangXuatAdmin') ;
Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function(){
	
	Route::group(['prefix'=>'theloai'],function(){
		Route::get('confirm-delete/{id}',['as'=>'theloai_confirm-delete','uses'=>'TheLoaiController@getModal']) ;
		Route::get('danhsach','TheLoaiController@getDanhSach');
		Route::get('them','TheLoaiController@getThem');
		Route::post('them','TheLoaiController@postThem');
		Route::get('sua/{id}','TheLoaiController@getSua');
		Route::post('sua/{id}','TheLoaiController@postSua');
		Route::get('xoa/{id}','TheLoaiController@getXoa')->name('xoa_theloai');
	});
	Route::group(['prefix'=>'loaitin'],function(){
		Route::get('confirm-delete/{id}',['as'=>'loaitin_confirm-delete','uses'=>'LoaiTinController@getModal']) ;
		Route::get('danhsach','LoaiTinController@getDanhSach');
		Route::get('them','LoaiTinController@getThem');
		Route::post('them','LoaiTinController@postThem');
		Route::get('sua/{id}','LoaiTinController@getSua');
		Route::post('sua/{id}','LoaiTinController@postSua');
		Route::get('xoa/{id}','LoaiTinController@getXoa')->name('xoa_loaitin');
	});
	Route::group(['prefix'=>'tintuc'],function(){
		Route::get('confirm-delete/{id}',['as'=>'tintuc_confirm-delete','uses'=>'TinTucController@getModal']) ;
		Route::get('danhsach','TinTucController@getDanhSach');
		Route::get('them','TinTucController@getThem');
		Route::post('them','TinTucController@postThem');
		Route::get('sua/{id}','TinTucController@getSua');
		Route::post('sua/{id}','TinTucController@postSua');
		Route::get('xoa/{id}','TinTucController@getXoa')->name('xoa_tintuc') ;
	});
	Route::group(['prefix'=>'slide'],function(){
		Route::get('confirm-delete/{id}',['as'=>'slide_confirm-delete','uses'=>'SlideController@getModal']) ;
		Route::get('danhsach','SlideController@getDanhSach');
		Route::get('them','SlideController@getThem');
		Route::post('them','SlideController@postThem');
		Route::get('sua/{id}','SlideController@getSua');
		Route::post('sua/{id}','SlideController@postSua');
		Route::get('xoa/{id}','SlideController@getXoa')->name('xoa_slide') ;
	});
	Route::group(['prefix'=>'user'],function(){
		Route::get('confirm-delete/{id}',['as'=>'confirm-delete','uses'=>'UserController@getModal']) ;
		Route::get('danhsach','UserController@getDanhSach');
		Route::get('them','UserController@getThem');
		Route::post('them','UserController@postThem');
		Route::get('sua/{id}','UserController@getSua');
		Route::post('sua/{id}','UserController@postSua');
		Route::get('xoa/{id}','UserController@getXoa')->name('delete_user') ;
	});
	Route::group(['prefix'=>'comment'],function(){
		Route::get('xoa/{id}/{idTinTuc}','CommentController@getXoa') ;
	});
	Route::group(['prefix'=>'ajax'],function(){
		Route::get('loaitin/{idTheLoai}','AjaxController@getLoaiTin');
	});
});
Route::get('trangchu','PagesController@trangchu') ;
Route::get('lienhe','PagesController@lienhe') ;
Route::get('loaitin/{id}/{TenKhongDau}.html','PagesController@loaitin') ;
Route::get('tintuc/{id}/{TieuDeKhongDau}.html','PagesController@tintuc') ;
Route::get('dangnhap','PagesController@getDangNhap') ;
Route::post('dangnhap','PagesController@postDangNhap') ;
Route::get('dangxuat','PagesController@getDangXuat') ;
Route::get('nguoidung','PagesController@getNguoiDung') ;
Route::post('nguoidung','PagesController@postNguoiDung') ;
Route::get('dangky','PagesController@getDangKy') ;
Route::post('dangky','PagesController@postDangKy') ;


Route::post('comment/{id}','CommentController@postComment') ;

Route::get('timkiem','PagesController@timkiem') ;
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
