<?php

/**
* Language file for user delete modal
*
*/
return array(

    'body'			=> 'Bạn có thực sự muốn xóa slide này',
    'cancel'		=> 'Thoát',
    'confirm'		=> 'Xóa',
    'title'         => 'Xóa tài thể loại',

);
