<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth ;
use App\TheLoai ;
use App\Slide ;
use App\LoaiTin ;
use App\TinTuc ;
use App\User ;
class PagesController extends Controller
{
    //
    function __construct()
    {
    	$theloai = TheLoai::all() ;
    	$slide = Slide::all() ;
    	view()->share('theloai',$theloai) ;
    	view()->share('slide',$slide) ;
    	if(Auth::check())
		{
			view()->share('nguoidung',Auth::user()) ;

		}
    }
    public function trangchu()
    {
    	//$theloai = TheLoai::all() ;
    	return view('pages.trangchu') ;
    		//,['theloai'=>$theloai]) ;
    }
    public function lienhe()
    {
    	return view('pages.lienhe') ;
    }
    public function loaitin($id)
    {
    	$loaitin = LoaiTin::find($id) ;
    	$tintuc = TinTuc::where('idLoaiTin',$id)->paginate(5) ;
        //$tintuc->TieuDeKhongDau = str_replace(".","",$tintuc->TieuDeKhongDau ) ;
    	return view('pages.loaitin',['loaitin'=>$loaitin,'tintuc'=>$tintuc]) ;
    }
    public function tintuc($id)
    {

    	$tintuc = TinTuc::find($id) ;
        
    	$tinnoibat = TinTuc::where('NoiBat',1)->take(4)->get() ;
    	$tinlienquan = TinTuc::where('idLoaiTin',$tintuc->idLoaiTin)->take(4)->get() ;

        $tintuc->TieuDeKhongDau = str_replace(".","",$tintuc->TieuDeKhongDau ) ;
        foreach ($tinnoibat as $key => $value) {
            $value->TieuDeKhongDau = str_replace(".","",$value->TieuDeKhongDau) ;
        }
        foreach ($tinlienquan as $key => $value) {
            $value->TieuDeKhongDau = str_replace(".","",$value->TieuDeKhongDau) ;
        }
      //echo $tinnoibat[0]->TieuDeKhongDau ;
      //die ;
    	return view('pages.tintuc',
    		['tintuc'=>$tintuc,'tinnoibat'=>$tinnoibat,'tinlienquan'=>$tinlienquan]) ;
    }
    public function getDangNhap()
    {
    	return view('pages.dangnhap') ;
    }
    public function postDangNhap(Request $request)
    {
    	$this->validate($request,
            [
                'email'=>'required',
                'password'=>'required|min:3|max:32'
            ],
            [
                'email.required'=>'Bạn chưa nhập email',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'mật khẩu không được nhỏ hơn 3 ký tự',
                'password.max'=>'mật khẩu không được lớn hơn 32 ký tự',
            ]) ;
    	if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            return redirect('trangchu') ;
        }else
        {
            return redirect('dangnhap')->with(['thongbao'=>'Đăng nhập thất bại']) ;
        }
    }
    public function getDangXuat()
    {
    	Auth::logout() ;
    	return redirect('trangchu') ;
    }
    public function getNguoiDung()
    {
    	return view('pages.nguoidung') ;

    }
    public function postNguoiDung(Request $request)
    {
    	$this->validate($request,
        [
            'name'=>'required|min:3',
            
        ],
        [
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'name.min'=>'tên người dùng có ít nhất 3 ký tự',
        ]);
        $user = Auth::user() ;
        $user->name = $request->name ;

        if($request->changePassword == "on"){
            $this->validate($request,
            [
                'password'=>'required|min:3|max:32',
                'passwordAgain'=>'required|same:password',
            ],
            [
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'mật khẩu phải có ít nhất 3 ký tự',
                'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp',
            ]);
            $user->password = bcrypt($request->password) ;
        }
        $user->save() ;
        return redirect('nguoidung')->with(['thongbao'=>'Bạn đã sửa thành công']) ;
       
    }
    public function getDangKy()
    {
        return view('pages.dangky') ;
    }
    public function postDangKy(Request $request)
    {
        $this->validate($request,
            [
                'name'=>'required|min:3',
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:3|max:32',
                'passwordAgain'=>'required|same:password',
            ],
            [
                'name.required'=>'Bạn chưa nhập tên người dùng',
                'name.min'=>'tên người dùng có ít nhất 3 ký tự',
                'email.required'=>'Bạn chưa nhập địa chỉ email',
                'email.email'=>'Tên email không đúng định dạng',
                'email.unique'=>'Địa chỉ email đã tồn tại',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'mật khẩu phải có ít nhất 3 ký tự',
                'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp',
            ]);
        $user = new User() ;
        $user->name = $request->name ;
        $user->email = $request->email ;
        $user->quyen = 0 ;
        $user->password = bcrypt($request->password) ;
        $user->save() ;
        return redirect('dangky')->with(['thongbao'=>'Bạn đã đăng ký thành công']) ;
    }
    public function timkiem(Request $request)
    {
        $tukhoa = $request->tukhoa ;
        $tintuc = TinTuc::where('TieuDe','like',"%$tukhoa%")->orWhere('TomTat','like',"%$tukhoa%")
                ->orWhere('NoiDung','like',"%$tukhoa%")->take(20)->paginate(5) ;
                //->skip(0)->take(20)->get() ; 
                //->paginate(5) ;                //->paginate(5) ;
        
        return view('pages.timkiem',['tintuc'=>$tintuc,'tukhoa'=>$tukhoa]) ;
    }
}
