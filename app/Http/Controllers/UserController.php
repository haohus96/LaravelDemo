<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException ;

use Exception;
use Illuminate\Support\Facades\Auth ;
use App\TheLoai ;
use App\LoaiTin ;
use App\TinTuc ;
use App\Slide ;
use App\User ;
use DB ;
class UserController extends Controller
{
    //
    public function getDanhSach()
    {
        $user = User::all() ;
        // DB::table('users')->chunk(5,function($users){
        //     echo 'test' ; die ;
        //     foreach($users as $use){
        //         echo '<pre>' ;
        //         var_dump($use) ;
        //         echo '</pre>' ;
        //     }
        // });
        // die ;
        return view('admin.user.danhsach',['user'=>$user]) ;
    }
    	
    public function getThem()
    {
    	return view('admin.user.them') ;
    }
    public function postThem(Request $request)
    {
    	$this->validate($request,
            [
                'name'=>'required|min:3',
                'email'=>'required|email|unique:users,email',
                'password'=>'required|min:3|max:32',
                'passwordAgain'=>'required|same:password',
            ],
            [
                'name.required'=>'Bạn chưa nhập tên người dùng',
                'name.min'=>'tên người dùng có ít nhất 3 ký tự',
                'email.required'=>'Bạn chưa nhập địa chỉ email',
                'email.email'=>'Tên email không đúng định dạng',
                'email.unique'=>'Địa chỉ email đã tồn tại',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'mật khẩu phải có ít nhất 3 ký tự',
                'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp',
            ]);
        $user = new User() ;
        $user->name = $request->name ;
        $user->email = $request->email ;
        $user->quyen = $request->quyen ;
        $user->password = bcrypt($request->password) ;
        $user->save() ;
        return redirect('admin/user/them')->with(['thongbao'=>'thêm user thành công']) ;
    }
    public function getSua($id)
    {
        $user = User::find($id) ;
        return view('admin.user.sua',['user'=>$user]) ;

    }  	
    public function postSua(Request $request,$id)
    {
        $this->validate($request,
        [
            'name'=>'required|min:3',
            
        ],
        [
            'name.required'=>'Bạn chưa nhập tên người dùng',
            'name.min'=>'tên người dùng có ít nhất 3 ký tự',
        ]);
        $user = User::find($id) ;
        $user->name = $request->name ;
        $user->quyen = $request->quyen ;

        if($request->changePassword == "on"){
            $this->validate($request,
            [
                'password'=>'required|min:3|max:32',
                'passwordAgain'=>'required|same:password',
            ],
            [
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'mật khẩu phải có ít nhất 3 ký tự',
                'password.max'=>'mật khẩu phải có nhiều nhất 32 ký tự',
                'passwordAgain.required'=>'Bạn chưa nhập lại mật khẩu',
                'passwordAgain.same'=>'Mật khẩu nhập lại chưa khớp',
            ]);
            $user->password = bcrypt($request->password) ;
        }
        $user->save() ;
        return redirect('admin/user/sua/'.$id)->with(['thongbao'=>'Bạn đã sửa thành công']) ;
       
    }
    public function getModal(Request $request)
    {
        $user_id = $request->id;
        $model = 'users' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('delete_user',['id'=>$user_id]) ;
            return view('admin.layout.modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;

        }catch (Exception $e){
            $error = "Lỗi xóa tài khoản" ;
            return view('admin.layout.modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;
        }

    }
    public function getXoa($id)
    {
       
        try{
            $user = User::find($id) ;
            $user->delete() ;
            return redirect('admin/user/danhsach')->with(['thongbao'=>'Bạn đã xóa thành công']) ;
            
        }catch (Exception $e){
            //echo "lỗi" ; die ;
            return redirect('admin/user/danhsach')->with(['thongbao'=>'Bạn không thể xóa tài khoản này']) ;
        }
    }
    public function getDangnhapAdmin()
    {
        return view('admin.login') ;
    }
    public function postDangnhapAdmin(Request $request)

    {

        $this->validate($request,
            [
                'email'=>'required',
                'password'=>'required|min:3|max:32'
            ],
            [
                'email.required'=>'Bạn chưa nhập email',
                'password.required'=>'Bạn chưa nhập mật khẩu',
                'password.min'=>'mật khẩu không được nhỏ hơn 3 ký tự',
                'password.max'=>'mật khẩu không được lớn hơn 32 ký tự',
            ]) ;
        $remember = ($request->remember_me == 'on') ? true : false;
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password],$remember)){
            return redirect('admin/theloai/danhsach') ;
        }else
        {
            return redirect('admin/dangnhap')->with(['thongbao'=>'Đăng nhập thất bại']) ;
        }
    }
    public function getDangXuatAdmin()
    {
        Auth::logout() ;
        return redirect('admin/dangnhap') ;
    }

}

