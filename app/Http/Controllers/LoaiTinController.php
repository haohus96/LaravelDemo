<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai ;
use App\LoaiTin ;
use Exception ;
class LoaiTinController extends Controller
{
    //
    public function getDanhSach()
    {
    	$loaitin = LoaiTin::all();
    	return view('admin.loaitin.danhsach',['loaitin'=>$loaitin]);
    }
    public function getThem()
    {
    	$theloai = TheLoai::all();
    	return view('admin.loaitin.them',['theloai'=>$theloai]);
    }
    public function postThem(Request $request)
    {
    	//echo $request->Ten ;
    	$this->validate($request,
    		[
    			'Ten'=>'required|unique:LoaiTin,Ten|min:3|max:100',
    			'TheLoai'=>'required'
    		],
    		[
    			'Ten.required'=>'Bạn chưa nhập tên loại tin',
    			'Ten.unique'=>'Tên loại tin đã tồn tại',
    			'Ten.min'=>'Tên loại tin có độ dài từ 3 đến 100 ký tự',
    			'Ten.max'=>'Tên loại tin có độ dài từ 3 đến 100 ký tự',
    			'TheLoai.required'=>'Bạn chưa chọn thể loại'
    		]);
    	$loaitin = new LoaiTin();
    	$loaitin->Ten = $request->Ten;
    	$loaitin->TenKhongDau = changeTitle($request->Ten);
    	$loaitin->idTheLoai = $request->TheLoai ;
    	$loaitin->save();
    	return redirect('admin/loaitin/them')->with(['thongbao'=>'Thêm thành công']);
    }
    public function getSua($id)
    {
        $loaitin = LoaiTin::find($id);
        $theloai = TheLoai::all();
    	return view('admin.loaitin.sua',['loaitin'=>$loaitin,'theloai'=>$theloai]);
    }
    public function postSua(Request $request,$id)
    {
        $this->validate($request,
    		[
    			'Ten'=>'required|unique:LoaiTin,Ten|min:3|max:100',
    			'TheLoai'=>'required'
    		],
    		[
    			'Ten.required'=>'Bạn chưa nhập tên loại tin',
    			'Ten.unique'=>'Tên loại tin đã tồn tại',
    			'Ten.min'=>'Tên loại tin có độ dài từ 3 đến 100 ký tự',
    			'Ten.max'=>'Tên loại tin có độ dài từ 3 đến 100 ký tự',
    			'TheLoai.required'=>'Bạn chưa chọn thể loại'
    		]);
    	$loaitin = LoaiTin::find($id);
    	$loaitin->Ten = $request->Ten;
    	$loaitin->TenKhongDau = changeTitle($request->Ten);
    	$loaitin->idTheLoai = $request->TheLoai ;
    	$loaitin->save();
    	return redirect('admin/loaitin/sua/'.$id)->with(['thongbao'=>'Bạn đã sửa thành công']);
    }
    public function getModal(Request $request)
    {
        $id = $request->id ;
        $model = 'LoaiTin' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('xoa_loaitin',['id'=>$id]) ;
            return view('admin.layout.modal_confirmation',['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;
        }catch(Exception $e){
            $error = "Không xoa được " ;
           
            return view('modal',['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;
        }
    }
    public function getXoa($id)
    {

       
        try{
            $loaitin = LoaiTin::find($id);
            $loaitin->delete();
            return redirect('admin/loaitin/danhsach')->with(['thongbao'=>'Xóa thành công']);
        }catch(Exception $e){
            return redirect('admin/loaitin/danhsach')->with(['thongbao'=>'Bạn không thể xóa loại tin này']);
        }
    }

}
