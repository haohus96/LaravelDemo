<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai ;
use App\LoaiTin ;
use App\TinTuc ;
use Exception ;
class TinTucController extends Controller
{
    //
    public function getDanhSach()
    {
    	$tintuc = TinTuc::orderBy('id','DESC')->get();
    	return view('admin.tintuc.danhsach',['tintuc'=>$tintuc]);
    }
    public function getThem()
    {
    	$theloai = TheLoai::all();
    	$loaitin = LoaiTin::all();

    	return view('admin.tintuc.them',['theloai'=>$theloai,'loaitin'=>$loaitin]);
    }
    public function postThem(Request $request)
    {
    	
    	$this->validate($request,
    		[
    			'LoaiTin'=>'required',
    			'TieuDe'=>'required|min:3|unique:TinTuc,TieuDe',
    			'TomTat'=>'required',
    			'NoiDung'=>'required'
    		],
    		[
    			'LoaiTin.required'=>'Bạn chưa chọn loại tin',
    			'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
    			'TieuDe.min'=>'Tiêu đề phải có ít nhất 3 ký tự',
    			'TieuDe.unique'=>'Tiêu đề đã tồn tại',
    			'TomTat.required'=>'Bạn chưa nhập tóm tắt',
    			'NoiDung.required'=>'Bạn chưa nhập nội dung'
    		]);
    	$tintuc = new TinTuc();
    	$tintuc->TieuDe = $request->TieuDe ;
    	$tintuc->TieuDeKhongDau = changeTitle($request->TieuDe) ;
    	$tintuc->idLoaiTin = $request->LoaiTin ;
    	$tintuc->TomTat = $request->TomTat ;
    	$tintuc->NoiDung = $request->NoiDung ;
    	$tintuc->SoLuotXem = 0;
    	$tintuc->NoiBat = $request->NoiBat ;
    	if($request->hasFile('Hinh')){
    		
    		$file = $request->file('Hinh') ;
    		$duoi = $file->getClientOriginalExtension() ;
    		if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
    			return redirect('admin/tintuc/them')->with(['thongbao'=>'Bạn chỉ được chọn file có đuôi jpg, png, jepg']) ;
    		}
    		$name = $file->getClientOriginalName();
    		$Hinh = str_random(4)."_".$name ;
    		while(file_exists("upload/tintuc/".$Hinh)){
    			$Hinh = str_random(4)."_".$name ;
    		}
    		$file->move("upload/tintuc",$Hinh) ;
    		$tintuc->Hinh = $Hinh ;
    	}
    	else
    	{

    		$tintuc->Hinh = "" ;
    	}
    	$tintuc->save() ;
    	return redirect('admin/tintuc/them')->with(['thongbao'=>'Bạn đã thêm thành công']) ;
    }
    public function getSua($id)
    {
       	$theloai = TheLoai::all() ;
    	$loaitin = LoaiTin::all() ;
    	$tintuc = TinTuc::find($id) ;
    	return view('admin.tintuc.sua',['tintuc'=>$tintuc,'theloai'=>$theloai,'loaitin'=>$loaitin]) ;
    }
    public function postSua(Request $request,$id)
    {
       $tintuc = TinTuc::find($id) ;
       $this->validate($request,
    		[
    			'LoaiTin'=>'required',
    			'TieuDe'=>'required|min:3|unique:TinTuc,TieuDe',
    			'TomTat'=>'required',
    			'NoiDung'=>'required'
    		],
    		[
    			'LoaiTin.required'=>'Bạn chưa chọn loại tin',
    			'TieuDe.required'=>'Bạn chưa nhập tiêu đề',
    			'TieuDe.min'=>'Tiêu đề phải có ít nhất 3 ký tự',
    			'TieuDe.unique'=>'Tiêu đề đã tồn tại',
    			'TomTat.required'=>'Bạn chưa nhập tóm tắt',
    			'NoiDung.required'=>'Bạn chưa nhập nội dung'
    		]);
    	
    	$tintuc->TieuDe = $request->TieuDe ;
    	$tintuc->TieuDeKhongDau = changeTitle($request->TieuDe) ;
    	$tintuc->idLoaiTin = $request->LoaiTin ;
    	$tintuc->TomTat = $request->TomTat ;
    	$tintuc->NoiDung = $request->NoiDung ;
    	$tintuc->NoiBat = $request->NoiBat ;
    	if($request->hasFile('Hinh')){
    		
    		$file = $request->file('Hinh') ;
    		$duoi = $file->getClientOriginalExtension() ;
    		if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
    			return redirect('admin/tintuc/them')->with(['thongbao'=>'Bạn chỉ được chọn file có đuôi jpg, png, jepg']) ;
    		}
    		$name = $file->getClientOriginalName();
    		$Hinh = str_random(4)."_".$name ;
    		while(file_exists("upload/tintuc/".$Hinh)){
    			$Hinh = str_random(4)."_".$name ;
    		}
    		if(file_exists("upload/tintuc/".$tintuc->Hinh)){
    			unlink("upload/tintuc/".$tintuc->Hinh) ;
    		}
    		$file->move("upload/tintuc",$Hinh) ;
    		$tintuc->Hinh = $Hinh ;
    	}
    
    	$tintuc->save() ;
    	return redirect('admin/tintuc/sua/'.$id)->with(['thongbao'=>'Bạn đã sửa thành công']) ;
    }
    public function getModal(Request $request)
    {
        $id = $request->id ;
        $model = 'TinTuc' ;
        $confirm_route = $error = null ;
        try{
            $confirm_route = route('xoa_tintuc',['id'=>$id]) ;
            return view('admin.layout.modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;

        }catch(Exception $e){

            $error = 'Không xóa được' ;
            return view('admin.layout.modal_confirmation',
                ['model'=>$model,'error'=>$error,'confirm_route'=>$confirm_route]) ;
        }
    }
    public function getXoa($id)
    {
        try{
            $tintuc = TinTuc::find($id) ;
            //var_dump($tintuc) ; die ;
            $tintuc->delete() ;
            return redirect('admin/tintuc/danhsach')->with(['thongbao'=>'Xóa thành công']) ;
        }catch(Exception $e){
            return redirect('admin/tintuc/danhsach')->with(['thongbao'=>'Bạn không thể xóa tin tức này']) ;
        }
      
    }

}

