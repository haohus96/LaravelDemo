<?php

namespace App\Http\Controllers;
use Exception ;
use Illuminate\Http\Request;
use App\TheLoai ;
class TheLoaiController extends Controller
{
    //
    public function getDanhSach()
    {
    	$theloai = TheLoai::all();
    	return view('admin.theloai.danhsach',['theloai'=>$theloai]);
    }
    public function getThem()
    {
    	return view('admin.theloai.them');
    }
    public function postThem(Request $request)
    {
    	//echo $request->Ten ;
    	$this->validate($request,
    		[
    			'Ten'=>'required|min:3:max:100|unique:TheLoai,ten'
    		],
    		[
    			'Ten.required'=>'Bạn chưa nhập gì',
    			'Ten.min'	=> 'Tên thể loại có độ dài từ 3 đến 100 kí tự',
    			'Ten.max'	=> 'Tên thể loại có độ dài từ 3 đến 100 kí tự',
                'Ten.unique'=>'Tên thể loại đã tồn tại',
    		]);
    	$theloai = new TheLoai();
        
    	$theloai->Ten = $request->Ten;
    	$theloai->TenKhongDau = changeTitle($request->Ten);
        
        
        // $theloai->insert([
        //                     'Ten' => $request->Ten,
        //                     'TenKhongDau' => changeTitle($request->Ten)
        //                     ]) ;
        // var_dump($theloai) ;
        // die ;
        
        
    	$theloai->save();
    	return redirect('admin/theloai/them')->with(['thongbao'=>'Thêm thành công']);
    }
    public function getSua($id)
    {
        $theloai = TheLoai::find($id);
    	return view('admin.theloai.sua',['theloai'=>$theloai]);
    }
    public function postSua(Request $request,$id)
    {
        $theloai = TheLoai::find($id);
        $this->validate($request,
            [
                'Ten'=>'required|unique:TheLoai,ten|min:3|max:100'
            ],
            [
                'Ten.required'=>'Bạn chưa nhập gì',
                'Ten.unique'=>'Tên thể loại đã tồn tại',
                'Ten.min'   => 'Tên thể loại có độ dài từ 3 đến 100 kí tự',
                'Ten.max'   => 'Tên thể loại có độ dài từ 3 đến 100 kí tự'
            ]);
        $theloai->Ten = $request->Ten ;
        $theloai->TenKhongDau = changeTitle($request->Ten) ;
        $theloai->save();
        return redirect('admin/theloai/sua/'.$request->id)->with(['thongbao'=>'Sửa thành công']);
    }
    public function getModal(Request $request)
    {
        $id = $request->id ;
        $model = 'TheLoai' ;
        $error = $confirm_route = null ;
        try{
            $confirm_route = route('xoa_theloai',['id'=>$id]) ;
           // echo $route_confirm ; die ;
            return view('admin.layout.modal_confirmation',
                ['error'=>$error,'model'=>$model,'confirm_route'=>$confirm_route]) ;

        }catch(Exception $e){
            $error = "Lỗi xóa thể loại" ;
            return view('admin.layout.modal_confirmation',
                ['error'=>$error,'model'=>$model,'confirm_route'=>$confirm_route]) ;
        }

    }
    public function getXoa($id)
    {
        try{
            $theloai = TheLoai::find($id);
            $theloai->delete();
            return redirect('admin/theloai/danhsach')->with(['thongbao'=>'Xóa thành công']);
        }catch(Exception $e){
            return redirect('admin/theloai/danhsach')->with(['thongbao'=>'Bạn không thể xó thể loại này']);
        }
        
    }

}
