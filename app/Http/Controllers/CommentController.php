<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth ;
use App\TheLoai ;
use App\LoaiTin ;
use App\Comment ;
use App\TinTuc ;
class CommentController extends Controller
{
    //
    public function getXoa($id,$idTinTuc)
    {
        $comment = Comment::find($id) ;
        $comment->delete();
        return redirect('admin/tintuc/sua/'.$idTinTuc)->with(['thongbao'=>'Xóa comment thành công']);
    }
    public function postComment(Request $request,$id)
    {
    	$tintuc = TinTuc::find($id) ;
    	$comment = new Comment() ;
    	$comment->idUser = Auth::user()->id ;
    	$comment->idTinTuc = $id ;
    	$comment->NoiDung = $request->NoiDung ;
       // var_dump($comment) ; die ;
    	$comment->save() ;
        $tintuc->TieuDeKhongDau = str_replace(".","",$tintuc->TieuDeKhongDau ) ;
    	return redirect("tintuc/$id/".$tintuc->TieuDeKhongDau.".html")->with(['thongbao'=>'Bạn đã bình luận']) ;
    }
}
